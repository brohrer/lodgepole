# lodgepole
Image and video processing toolbox

## Dependencies

Lodgepole relies on the [FFMPEG](https://ffmpeg.org/)
library when working with video.
It also makes heavy use of Numpy and Matplotlib.

## Installation

To use it:

```bash
python3 -m pip install numpy
python3 -m pip install numba
git clone https://gitlab.com/brohrer/lodgepole.git
python3 -m pip install -e lodgepole
```

## About the name

![Lodgpole pine](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Lodgepole%2C_looking_east.jpg/324px-Lodgepole%2C_looking_east.jpg)

Public Domain, [photo by Chris Dolanc](https://en.wikipedia.org/wiki/File:Lodgepole,_looking_east.jpg)

The tools are named after the Lodgepole Pine. There's no particular reason for that, but 
it sounds good goes
well with Cottonwood and Ponderosa.
